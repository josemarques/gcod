#ifndef GCOD_HPP_INCLUDED
#define GCOD_HPP_INCLUDED

#include <Arduino.h>

class GCOD{//Clase de GCODE, contiene la informaci�n codificada y decodificada.
private:
int FIND_IDENT(String ID, String MESS);
    
public:
//Declaraci�n de variables para control del motor
String dec="";//Last message to decode.
String cod="";//Last message coded.

//Declaraci�n de variables para decodificacor de instrucciones
bool G_90=true;//Modo G90 activo
bool G_91=false;//Modo G91 desactivado
bool G_92=false;//Instrucci�n de poner a 0 no activada
bool G_28=false;//Instrucci�n G28 no activa

bool M100=false;//Instrucción de medición de muestra, mide la muestra y se envía por USB el dato leído

//Declaraci�n de variables para decodificacor de instrucciones

int INST=0; //Estado de instrucci�n, 0 completado, 1 ejecutandose.

    GCOD();//Constructor sin estado
    GCOD(bool g90,bool g91, bool g92, bool g28);//Constructor con estado
    GCOD(const GCOD& gc);//Duplicador de clase
    GCOD& operator=(const GCOD& gc); //Igualador de clase
	~GCOD();//Destructor
    
    void SETP();
    
	void DECODE(String &msg,double &xd, double &spd, int &tmm);//Decodificador de instrucciones, mensaje recibido, posición[mm], velocidad[mm/s], tiempo de medida del sensor[s*10^-3]
    
    void CODE(String denm, double& x);
    
    //BASE GCOD CODEC_DECODEC
    double DEc(String ID, String MESS);//Returns the value on the string with the identificaror defined
    
    String COd(String ID, double VALUE);//Generates a string whith the data codificated
    
    //BASE GCOD CODEC_DECODEC
    
    

};
#endif
